import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {
    var config = {
      apiKey: "AIzaSyC_bY3zWtIXbnhMME-0jvFZ0Yhq1_RIxFM",
      authDomain: "agora-4296d.firebaseapp.com",
      databaseURL: "https://agora-4296d.firebaseio.com",
      projectId: "agora-4296d",
      storageBucket: "agora-4296d.appspot.com",
      messagingSenderId: "307374244238"
    };
    firebase.initializeApp(config);
  }
}
