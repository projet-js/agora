import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { HeaderComponent } from './header/header.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoFormComponent } from './todo-list/todo-form/todo-form.component';
import { SingleTodoComponent } from './todo-list/single-todo/single-todo.component';
import { AuthService } from './services/auth.service';
import { TodosService } from './services/todos.service';
import { AuthGuardService } from './services/auth-guard.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'auth/signup', component: SignupComponent},
  { path: 'auth/signin', component: SigninComponent},
  { path: 'todo-list', canActivate: [AuthGuardService], component: TodoListComponent},
  { path: 'todo-list/new', canActivate: [AuthGuardService], component: TodoFormComponent},
  { path: 'todo-list/view/:id', canActivate: [AuthGuardService], component: SingleTodoComponent},
  { path: '', redirectTo: 'todo-list', pathMatch: 'full' },
  { path: '**', redirectTo: 'todo-list'}
]

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    HeaderComponent,
    TodoListComponent,
    TodoFormComponent,
    SingleTodoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    TodosService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
