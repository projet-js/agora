export class Todo {
    actor: string;
    level: string;
    constructor(public title: string, public content: string) {}
}